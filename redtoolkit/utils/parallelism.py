from enum import Enum
from joblib import delayed, Parallel


class WorkerLevel(Enum):
    SOFT = 1
    HARD = 2


class WorkersPool(Parallel):

    def __init__(self, level=WorkerLevel.SOFT, size=-1, verbose=False):
        super(WorkersPool, self).__init__(
            n_jobs=size,
            prefer="processes" if level == WorkerLevel.HARD \
                else "threads",
            require=None if level == WorkerLevel.HARD \
                else "sharedmem",
            verbose=100 if verbose else 0
        )

    def perform_job(self, items, task, *task_args, **task_kwargs):
        return self(delayed(task)(item, *task_args, **task_kwargs) for item in items)


def hire(level=WorkerLevel.SOFT, n_workers=-1, verbose=False):
    return WorkersPool(level=level, size=n_workers, verbose=verbose)


def perform_job(items, task, level=WorkerLevel.SOFT, n_workers=-1, 
    verbose=False, *task_args, **task_kwargs):

    pool = hire(level=level, n_workers=n_workers, verbose=verbose)
    return pool.perform_job(items, task, *task_args, **task_kwargs)
