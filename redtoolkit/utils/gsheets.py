import pandas as pd


def get_as_df(gsheet_id, sheet_name=None):

    export_url = "https://docs.google.com/spreadsheets/d/%s/gviz/tq?tqx=out:csv" % gsheet_id

    if sheet_name:
        export_url += "&sheet=%s" % sheet_name

    return pd.read_csv(export_url)
