

def test_import_redtoolkit():
    try:
        import redtoolkit

        dir(redtoolkit.scidata.dataframes)
        dir(redtoolkit.scidata.graphs)
        dir(redtoolkit.scidata.vectors)

        dir(redtoolkit.textanalysis.similarities.wordnet)

        dir(redtoolkit.utils.gsheets)
        dir(redtoolkit.utils.parallelism)
        dir(redtoolkit.utils.spacy)

        redtoolkit.visualizers

        redtoolkit.wrappers

    except (AttributeError, ImportError, ModuleNotFoundError):
        assert(False)

    assert(True)


def test_import_from_redtoolkit():
    try:
        from redtoolkit import scidata
        dir(scidata.dataframes)
        dir(scidata.graphs)
        dir(scidata.vectors)

        from redtoolkit import textanalysis
        dir(textanalysis.similarities.wordnet)

        from redtoolkit.textanalysis import similarities
        dir(similarities.wordnet)

        from redtoolkit.textanalysis.similarities import wordnet
        dir(wordnet)

        from redtoolkit import utils
        dir(utils.gsheets)
        dir(utils.parallelism)
        dir(utils.spacy)

        from redtoolkit import visualizers

        from redtoolkit import wrappers

    except (AttributeError, ImportError, ModuleNotFoundError):
        assert(False)

    assert(True)