import networkx as nx
import pandas as pd


__all__ = [
    "convert_df_to_graph",
    "convert_adjacency_graph_to_df",
    "convert_df_to_adjacency_df"
]


def convert_df_to_graph(df, asymmetric=False):
    return (nx.DiGraph if asymmetric else nx.Graph)(df)


def convert_adjacency_graph_to_df(graph):
    return pd.DataFrame(
        [[a, b, data['weight']] for a, b, data in graph.edges(data=True)], 
        columns=['a', 'b', 'w'])


def convert_df_to_adjacency_df(df, asymmetric=False):
    return convert_adjacency_graph_to_df(convert_df_to_graph(df, asymmetric))
