from redtoolkit.scidata import graphs
from redtoolkit.utils import gsheets
from redtoolkit.utils import parallelism
from redtoolkit.utils import spacy
import pandas as pd


__all__ = [
    "ranking_by_embeddings_from_gsheet",
    "ranking_by_embeddings_from_df"
]


def ranking_by_embeddings_from_gsheet(
    gsheet_id,
    model=spacy.SPACY_MD, parallelisation=parallelism.WorkerLevel.SOFT, sorting=False,
    column=None, sheet_name=None, limit=None, save_csv=None
):
    df = gsheets.get_as_df(gsheet_id, sheet_name=sheet_name)
    return ranking_by_embeddings_from_df(
        df,
        model=model, parallelisation=parallelisation, sorting=sorting,
        column=column, sheet_name=sheet_name, limit=limit, save_csv=save_csv
    )


def ranking_by_embeddings_from_df(
    df,
    model=spacy.SPACY_MD, parallelisation=parallelism.WorkerLevel.SOFT, sorting=False,
    column=None, sheet_name=None, limit=None, save_csv=None
):
    if column:
        df = pd.DataFrame(df[column])

    if limit:
        df = df.head(limit)

    items = [r[1].lower() for r in df.itertuples()]

    nlp = spacy.load_model(model)

    results = parallelism.perform_job(items, _iteration_task, 
        level=parallelisation, all_items=items, nlp=nlp
    )

    df = graphs.convert_df_to_adjacency_df(pd.concat(results, sort=True))

    if sorting:
        df.sort_values(by='w', ascending=False, inplace=True)

    if save_csv:
        df.to_csv(save_csv)

    return df


def _iteration_task(src_item, all_items, nlp):
    a_nlp = nlp(src_item)
    row = {
        b_nlp.text: b_nlp.similarity(a_nlp) \
            for b_nlp in nlp.pipe(all_items) \
                if b_nlp.text != src_item
    }
    return pd.DataFrame(row, index=[src_item])
