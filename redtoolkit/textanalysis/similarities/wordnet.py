from collections import defaultdict
from nltk.corpus import wordnet as wn
from redtoolkit.scidata import graphs
from redtoolkit.utils import gsheets
from redtoolkit.utils import parallelism
import pandas as pd
import re

import nltk
nltk.download("wordnet")


__all__ = [
    "ranking_by_wordnet_from_gsheet",
    "ranking_by_wordnet_from_df"
]


def ranking_by_wordnet_from_gsheet(
    gsheet_id, 
    parallelisation=parallelism.WorkerLevel.SOFT, sorting=False,
    column=None, sheet_name=None, limit=None, save_csv=None
):
    df = gsheets.get_as_df(gsheet_id, sheet_name=sheet_name)
    return ranking_by_wordnet_from_df(
        df, 
        parallelisation=parallelisation, sorting=sorting,
        column=column, limit=limit, save_csv=save_csv
    )


def ranking_by_wordnet_from_df(
    df,
    parallelisation=parallelism.WorkerLevel.SOFT, sorting=False,
    column=None, limit=None, save_csv=None
):
    if column:
        df = pd.DataFrame(df[column])

    if limit:
        df = df.head(limit)

    items = [r[1].lower() for r in df.itertuples()]

    if parallelisation == parallelism.WorkerLevel.HARD:
        # force wordnet to be loaded for multiprocessing
        wn.ensure_loaded() 

    results = parallelism.perform_job(
        items, _iteration_task, 
        level=parallelisation, all_items=items
    )

    df = graphs.convert_df_to_adjacency_df(pd.concat(results, sort=True))

    if sorting:
        df.sort_values(by='w', ascending=False, inplace=True)

    if save_csv:
        df.to_csv(save_csv)

    return df


def _iteration_task(src_item, all_items):

    a_syns = []

    for word in re.split(r' ', src_item.lower()):
        try:
            syns = wn.synsets(word)
        except:
            continue

        a_syns.append(set(syns))

    row = defaultdict(int)
    for b in all_items:

        if b == src_item:
            continue

        row[b] = 0

        for syns in a_syns:
            for word in re.split(r' ', b.lower()):
                try:
                    b_syns = wn.synsets(word)
                except:
                    continue

                row[b] += len(syns.intersection(set(b_syns)))

    return pd.DataFrame(row, index=[src_item])