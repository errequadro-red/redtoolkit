


if __name__ == "__main__":
    from os.path import dirname
    import sys
    
    # fix root module importing
    sys.path.append(dirname("."))
    
    from redtoolkit.textanalysis import embeddings
    from redtoolkit.textanalysis import wordnet
    from redtoolkit.utils import spacy
    import fire

    # Commands exposed to CLI

    fire.Fire({

        "ranking_by_embeddings_from_gsheet": embeddings.ranking_by_embeddings_from_gsheet,
        "ranking_by_wordnet_from_gsheet": wordnet.ranking_by_wordnet_from_gsheet,
        "spacy_download": spacy.download_model,

    })